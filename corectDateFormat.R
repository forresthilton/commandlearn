
path <- "~/.commandLearnCards.json"

library(jsonlite)
workingCopy <- fromJSON(path  ,simplifyVector = TRUE)


save <- function() {
  sink(path)
  print(toJSON(workingCopy,pretty = T))
  sink()
}

## for the editing of history 

change <- function(history) {
  editedTimes <- NULL
  for ( time in history$timeStarted) {
    dateTime <- as.POSIXct(time, origin="1970-01-01",optional=T)
    if (is.na(dateTime)) {
      dateTime <- as.POSIXct(as.numeric(time), origin="1970-01-01",optional=F)
    }
    editedTimes <- c(editedTimes, as.numeric(dateTime))
  }
  print(history$timeStarted)
  print(editedTimes)
  history$timeStarted <- editedTimes
  return(history)
}

for (numInWorking in 1:length(workingCopy)) {
  inSet <- length(workingCopy[[numInWorking]])
  for (numInSet in 1:inSet) {
    workingCopy[[numInWorking]][[numInSet,"history"]] <- change(workingCopy[[numInWorking]][[numInSet,"history"]])
  }
}
