
	commandLearn is a command line flash card program that runs locally and stores usage data across sessions. The purpose of the project is to use the tools of R to implement spaced repetition and to manage digital flash card sets.

To install, download and run installer.*.sh. It is advisable to read the installer before running. The program is installed as /usr/bin/cml.

during practice (cml practice <set>...), the front of the card is printed then input is received. Entering all white space will indicate that you know and do not need to check. "f" as in flip or an inexact answer to the card results in input being received again after the back of the card is shown. At this time it is essential for you to enter "c" to indicate that you knew the answer if indeed you did. If you did not you can enter anythings or nothing.

commands used during practice:

h - see this list
f - flip
c - as correct. indicate that you knew the card
k - skip card
u,b - undo input
r - reorder cards. see description of spaced repitition below
q - quit program

The default spaced repetition algorithm that, "basic", just orders them by how urgent they are. After you do some you may forget the first and so you should use "r" to re order and revisit those.

  To add sets, use "cml add <set>". The command reads the set from stdin in a alternating term then definition format. The default separator is the line ending but an alternative can be provided using -s <char>. Empty lines will be ignored. The set must be ended with an end of file (EOF) signal. 


