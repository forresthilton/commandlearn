#!/usr/bin/env Rscript
# Copyright 2019 Forrest Hilton 
# avalible under the GNU GPLv3 (lisens in repository)
# if you would like to make modificaitions you should clone the git repositrory 
# thought it is not strictly needed. Pleas share added featurs.

# varables for lietner sistem:


rasheo  <- 4 # for the geametrick searies of boxes
# this implemintaition of lietner buts each card in a box and orders al the cards by the rasho of how long it has been since the last usage of the card to how often the card should be used.
nullCaseReturn  <-  25 # the return in the case that the card has never been used 


# this is the only file that runs, read at your owen risk.

# rm(list=ls())

library(docopt)


    "
Usage:
  cml list
  cml add [-s <char>] <set>
  cml export [-s <char>] <set> 
  cml view <set>
  cml practice [-r] [-a <algarithem>] [-d <data>] [--] <set>...
  cml split <source> <start> <end> <target>
  cml rename <source> <target>
  cml combine <source>... <target>
  cml --version
  cml --help

Options:
  -r, --reversed                              shows definition before the term. 
  -e <time>, --end <time>                     end time in minuts from start                                
  -a --algarithem <algarithem>                spesify an algarithem name, one of leitner overdue or basic [default: basic]
  -d <data>, --data <data>                    string Data you choos to provide about the seshion

  -s <char>, --seperaitor <char>              spesify the seporator characture default is new line caracture

  -h --help       Show this screen.
  --version       Show version.
" -> doc

if (interactive()) {
    args <- "practice -a leitner ajentesvoc "
} else {
    args <- commandArgs(TRUE)
}


if (ifelse(length(args) >= 3,args[1] == "combine",F )) {
    #weird case where docopt is insufishent
    commandInfo <- docopt(doc,version="3.0",args=c("rename","fill","fill"))
    commandInfo$target <- args[length(args)]
    commandInfo$source <- args[c(-1,-length(args))]
} else {
    commandInfo <- docopt(doc,version="3.0",args=args)
}

rm(args,doc)

detach("package:docopt", unload = TRUE)

library(methods)
library(assertthat)
library(jsonlite)




if (commandInfo$practice) {

    setings <- list(
    practicReversed= commandInfo$reversed,
    plandEnd= commandInfo$end,
    continueosRepitishon= commandInfo$algarithem %in% c("leitner"),
    algerithemName= commandInfo$algarithem,
    seshionInfo= commandInfo$data,
    setNames= commandInfo$set
    )
    rm(commandInfo)
    if (is.null(setings$seshionInfo)) {setings$seshionInfo <- NA}
    if (is.null(setings$plandEnd)) {setings$plandEnd <- NA}

    
    library(parallel)
    library(stringr)
    library(plyr)



                                        # maine copy

    Card <- setRefClass("Card", fields = c("history","definition","term"))

    mainCopy <- list()


    fromRestore <- fromJSON("~/.commandLearnCards.json",simplifyVector = TRUE)
    # not removed because is used in calibration.

    formatSet <- function(set) apply(set,MARGIN = 1,FUN = function(X) Card$new(history = X$history,definition = X$definition, term = X$term) )     
    for(name in names(fromRestore)) {
        mainCopy[[name]] <- formatSet(fromRestore[[name]])
    }
    rm(formatSet,name)
    
                                        # save maneCopy

    save <- function() {
        if(interactive()){return()}
        # I definity need to do this asink.
  
        convertSet <- function(set) {
            value <- sapply(set ,   USE.NAMES = T,
                             FUN = function(X) list(history = X$history,definition = X$definition, term = X$term)
                            )
            value <- as.data.frame(t(value))
            history <- c()
            for (item in value$history) {
                history[[length(history)+1]] <- item
            }
            
            return( data.frame(
                history = I(history),
                definition = as.character(unlist(value$definition)),
                term = as.character(unlist(value$term))
            ) )
        }
        
        savingCopy <- mainCopy
        
        for(name in names(savingCopy)) {
            savingCopy[[name]] <- convertSet(savingCopy[[name]])
        }
        
        sink("~/.commandLearnCards.json")
        print(toJSON(savingCopy,pretty = T))
        sink()
    }

    

                                        # analisis frame
    
    
    anilisingCopy <- list()
    for(name in setings$setNames)  {
        assert_that(!is.null(mainCopy[[name]]))
        anilisingCopy <- c(anilisingCopy,mainCopy[[name]])
    }
    
    anilisisFrame <- data.frame(row.names= 1:length(anilisingCopy),
                                card = rep(NA,length(anilisingCopy)),
                                length = sapply(anilisingCopy,FUN = function(card) nrow(as.data.frame(card$history)) ) 
                                )
    
    anilisisFrame$card = anilisingCopy
    rm(anilisingCopy)
    

    
    orderRows <- function() {
        v <- unlist(sapply(1:nrow(anilisisFrame),updateRow))
        anilisisFrame[1:nrow(anilisisFrame),"cerentRating"] <<- v
        anilisisFrame <<- anilisisFrame[order(anilisisFrame$cerentRating,decreasing = T),]
    }
    
    choose <- function() {#returns Card
        choice <- anilisisFrame[[1,"card"]]
        anilisisFrame <<- anilisisFrame[c(2:nrow(anilisisFrame),1),]
        return(choice)
    }

    roleBackOne <- function() { # not used
        n <- nrow(anilisisFrame)
        anilisisFrame <<- anilisisFrame[c(n,1:(n-1)),]
    }

    
    
    
    

# analisis ades

    cerentTime <- function() as.numeric(as.POSIXct(Sys.time()))
    
    wasRight <- function(outcome) {
        assert_that(all(outcome %in% c("wrong","didentKnow","confident","knew","right") ) )
        return(outcome %in% c("confident", "knew", "right"))
    }

    delta <- function(df) {
        times <- as.numeric(as.POSIXct(df$timeStarted, origin="1970-01-01"))
        befor <- c(-Inf,times)
        befor <- befor[-length(befor)]
        after <- times
        return(after - befor)
    }
    
    streek <- function(history) {#dose not remove values of the difrent reversed value
        if(nrow(as.data.frame(history)) == 0 ) {return(0)}
        v <- rev(wasRight(history[,"outcome"]))
        first <- v[1]
        i <- 1 
        while(isTRUE( (v==first)[i+1] ))  {i <- i+1}
        if (!first) {i <- -i}
        return(i)
    }
    

#algorithems


    algarithems <- list(# not vectorised
        overdue = function(rowN) {
            history <- anilisisFrame[[rowN,"card"]]$history
            if (nrow(history)==0) return(0)
            return(1/history[[nrow(history),"timeStarted"]])
        },
        basic = function(rowN) {
            history <- anilisisFrame[[rowN,"card"]]$history
            
            if ( length(history) != 0) {
                history <- history[which(setings$practicReversed==history$practicReversed),]
                history <- history[which(delta(history) > 7),]# removing ones within seven secont of a preveos use
                
            }
            
            streek <- streek(history)
            
            # if (streek == 1 && nrow(history)==1) {return(-2.5)}
            if (streek==0) {return(-1.5)}
            return(-streek)
        },
        leitner = function(rowN) {
            history <- anilisisFrame[[rowN,"card"]]$history
            
            if ( length(history) != 0) {
                history <- history[which(setings$practicReversed==history$practicReversed),]
                history <- history[which(delta(history) > 7),]# removing ones within seven secont of a preveos use    
            }

            

            timeSinse <- cerentTime() - history[[nrow(history),"timeStarted"]]

            if(length(timeSinse)!=1) {return(nullCaseReturn)}

            s <- streek(history)
            box <- ifelse(s>0,s,0)
            return(timeSinse/rasheo^box)
        }
    )

    true <- assert_that(setings$algerithemName %in% names(algarithems) ,msg="invalid algarithem name")

    updateRow <- algarithems[[setings$algerithemName]]



                                        # undo system

    previos <- list()
    
    done <- function(choice) { # called only at the end of do
        previos <<- c(previos, choice)
    }
    
    undo <- function() {
        lastCard <- previos[[length(previos)]]
        lastCard$history <- lastCard$history[-nrow(lastCard$history),]
        previos <<- previos[-length(previos)]
        doCard(lastCard)
    }

    




                                        #do card

    
    readIn <- function() {
        if(interactive()) {
            readline()
        }else {
            readLines("stdin",n=1)
        }
    }

    tell <- function(str){
        cat(str_c(str,"\n"))
    }

    print.help <- function() {
        tell("
h - see this list
f - flip
c - as correct. indicait that you knew the card
k - skip card
u,b - undo input
q - quit program
r - reorder
")
    }
    
    compare <- function(ab) {
        assert_that(length(ab)==2)
        ab <- tolower(str_trim(ab))
        return(ab[1]==ab[2])
    }

    
    
    doCard <- function(choice) {
  #sincronosly dose the card, consults setings as needed.
  
        sides <- c(choice$term,choice$definition)
        if (setings$practicReversed) {sides <- rev(sides)}
        tell(sides[1])
        
        startTime <- cerentTime()
        timeFliped <- NA
        outcome <- NA
  
        respons <- readIn()
  
                                        #switch
        if ("h"==respons) {
            print.help()
        } else if ("q"==respons) {
            shouldContinue <<-F
            return()
        } else if ("r"==respons) {
            orderRows()
            return()
        } else if ("k"==respons) {
            return()
        } else if (any(respons==c("u","b"))) {
            undo()
            doCard(choice)
            return()
        } else if ( "f" == respons ) {
            timeFliped <- cerentTime()
            outcome <- "didentKnow"
            tell(sides[2])
            respons <- readIn()
        
        } else if (compare(c(respons,sides[2]))) {
            outcome <- "right"
            tell("correct!")
        } else if("" == respons) {
            outcome <- "confident"
        } else {
            timeFliped <- cerentTime()
            outcome <- "wrong"
            tell(sides[2])
            respons <- readIn()
        } 
        timeEnded <- cerentTime()
        
        assert_that(wasRight(outcome) == is.na(timeFliped))
        
    # handaling the secont read line
        if ("h"==respons) {
            print.help()
        } else if ("q"==respons) { 
            shouldContinue <<-F
            return()
        } else if ("r"==respons) {
            orderRows()
            return()
        } else if (any(respons==c("u","b"))) {
            doCard(choice)
            return()
        } else if ("c"==respons ) {
            outcome <- "knew"
        } 
        
        
        assert_that(!is.na(outcome) )
        assert_that(outcome %in% 
                    c("wrong","didentKnow","confident","knew","right"))
  
                                        #report
        reciet <- data.frame(
            timeStarted = startTime,
            timeBeforFlip = timeFliped-startTime,
            timeTook = timeEnded-startTime,
            outcome = outcome,
            plandEnd = setings$plandEnd,
            practicReversed = setings$practicReversed,
            algerithemName = setings$algerithemName,
            seshionInfo = setings$seshionInfo
        )
        
        choice$history <- rbind.fill(as.data.frame(choice$history),
                                     as.data.frame(reciet))
        
        done(choice)
    }
    
    
    

    
# loop
    
    orderRows()
    choice <- choose()
    
    shouldContinue <- T 
    
    while(shouldContinue) {
        assert_that(!interactive())
        cardInProgres <- choice
        
        saveTask <- mcparallel(expr = save(),name = "saver")
        doCard(choice)
        
        mccollect(saveTask)

        
        if(setings$continueosRepitishon) {
            orderRows()
        }
        
        choice <- choose()
        
        if (identical(choice,cardInProgres)) {choice <- choose()}
        
    }
    
    save()
    
    q()# end of practice #######################################
}




# other comands and the common functions of the

readIn <- function() {
    if(interactive()) {
        readline()
    } else {
        readLines("stdin",n=1)
    }
}

workingCopy <- fromJSON("~/.commandLearnCards.json"
                       ,simplifyVector = TRUE)

save <- function() {
    sink("~/.commandLearnCards.json")
    print(toJSON(workingCopy,pretty = T))
    sink()
}




if (commandInfo$add) {
    library(stringr)
    seperator <- commandInfo$seperaitor
    if(is.null(seperator)) {seperator <- "\n"}
    
    input <- readLines("stdin",n=-1,)
    soFar <- ""
    for (line in input) {
        if (soFar!="") {
            soFar <- str_c(soFar, "\n")
        }
        soFar <- str_c(soFar,line)
    }
    
    v <- strsplit(soFar,seperator)[[1]]
    disalowed <- which(str_trim(v) == "")
    
    if(length(v)==0) {
        v <- v[-disalowed]
    }
    
    num <- length(v)
    true <- assert_that(num!=0,msg="Error: no cards")
    true <- assert_that(num %% 2 == 0,msg = "Error: not all cards have a defition")
    
    even <- 2*1:(num/2)
    odd <- even -1
    
    set <- data.frame(
        term = v[odd],
        definition = v[even],
        history =  I(rep( list(list()) , num/2 ) )
    )
    
    workingCopy[[commandInfo$set]] <- set
    save()
} else if (commandInfo$export) {
    library(stringr)
    seperator <- commandInfo$seperaitor
    if(is.null(seperator)) {seperator <- "\n"}
    
    set <- workingCopy[[commandInfo$set]] 
    
    soFar <- ""
    for (rowi in 1:nrow(set)) {
        if ("" != soFar) {soFar <- str_c(soFar,seperator)}
        soFar <- str_c(soFar,set[[rowi,"term"]],seperator,set[[rowi,"definition"]]) 
    }
    
    cat(soFar)
    cat("\n")
} else if (commandInfo$list) {
    
    print(names(workingCopy))
    
} else if (commandInfo$view) {
    set <- workingCopy[[commandInfo$set]]    
    
    set$history <- sapply(set$history,function(x) nrow(x))
    
    print(set[,c("term","definition","history")])
                                        #prints in specific order
} else if (commandInfo$split) {
    source <- workingCopy[[commandInfo$source]]
    start <- as.numeric( commandInfo$start )
    end <- as.numeric( commandInfo$end )
    assert_that( 1 <= start )
    assert_that( end <= length(source) )
    source <- source[-(start:end),]
    target <- source[(start:end),]
    workingCopy[[commandInfo$source]] <- source
    workingCopy[[commandInfo$target]] <- target
    save()
} else if (commandInfo$rename || commandInfo$combine) {
    target <- NULL
    for (set in commandInfo$source) {
        target <- rbind(target,workingCopy[[set]])
        workingCopy[[set]] <- NULL
    }
    workingCopy[[commandInfo$target]] <- target
    save()
}




